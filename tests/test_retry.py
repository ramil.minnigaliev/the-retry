import statistics
import time
import pytest
from typing import Callable, Type, Union
from unittest.mock import AsyncMock, Mock

from the_retry import retry


BACKOFF = 0.001  # Base backoff parameter for tests.


def get_exponential_backoff_time(attemps: int, backoff: Union[int, float]) -> Union[int, float]:
    return sum([backoff * 2 ** a for a in range (attemps - 1)])


def create_named_mock(mock_type: Union[Type[Mock], Type[AsyncMock]]) -> Callable:
    mock = mock_type(side_effect=RuntimeError())
    mock.__name__ = "failed"
    return mock


@pytest.fixture
def failed():
    return create_named_mock(Mock)


@pytest.fixture
async def async_failed():
    return create_named_mock(AsyncMock)


def test_retry__expected_exceptions(failed):
    decorator = retry((ValueError, RuntimeError))
    decorated = decorator(failed)
    with pytest.raises(RuntimeError):
        decorated()
    assert failed.call_count > 1


def test_retry__expected_exceptions__no_retry(failed):
    """check not retry if raised exception is not expected."""
    decorator = retry(ValueError)
    decorated = decorator(failed)
    with pytest.raises(RuntimeError):
        decorated()
    assert failed.call_count == 1


def test_retry__attempts(failed):
    attempts = 3
    decorator = retry(RuntimeError, attempts=attempts)
    decorated = decorator(failed)
    with pytest.raises(RuntimeError):
        decorated()
    assert failed.call_count == attempts


def test_retry__backoff(failed):
    backoff = BACKOFF
    decorator = retry(RuntimeError, backoff=backoff)
    decorated = decorator(failed)
    time_start = time.time()
    with pytest.raises(RuntimeError):
        decorated()
    estimated_time = time.time() - time_start
    assert backoff < estimated_time < backoff * 2


@pytest.mark.asyncio()
async def test_retry__backoff__async(async_failed):
    backoff = BACKOFF
    decorator = retry(RuntimeError, backoff=backoff)
    decorated = decorator(async_failed)
    time_start = time.time()
    with pytest.raises(RuntimeError):
        await decorated()
    estimated_time = time.time() - time_start
    assert backoff < estimated_time < backoff * 2


def test_retry__exponential_backoff(failed):
    attempts = 3
    backoff = BACKOFF
    decorator = retry(RuntimeError, attempts = attempts, backoff=backoff, exponential_backoff=True)
    decorated = decorator(failed)
    time_start = time.time()
    with pytest.raises(RuntimeError):
        decorated()
    estimated_time = time.time() - time_start
    expected_time = get_exponential_backoff_time(attempts, backoff)
    assert expected_time < estimated_time < expected_time + backoff


def test_retry__ignore_exceptions(failed):
    decorator = retry(RuntimeError, ignore_exceptions=True)
    decorated = decorator(failed)
    decorated()
    assert failed.call_count > 1


def test_retry__jitter(failed):
    attempts = 2
    backoff = BACKOFF
    decorator = retry(RuntimeError, attempts = attempts, backoff=backoff, jitter=backoff)
    decorated = decorator(failed)
    estimated = []
    for _ in range(10):
        time_start = time.time()
        with pytest.raises(RuntimeError):
            decorated()
        estimated.append(time.time() - time_start)
    assert statistics.stdev(estimated) > backoff * 0.1


def test_retry__maximum_backoff(failed):
    attempts = 4
    backoff = BACKOFF
    decorator = retry(
        RuntimeError, attempts = attempts, backoff=backoff, exponential_backoff=True, 
        maximum_backoff=backoff * 2,
    )
    decorated = decorator(failed)
    time_start = time.time()
    with pytest.raises(RuntimeError):
        decorated()
    estimated_time = time.time() - time_start
    expected_time = get_exponential_backoff_time(attempts, backoff)
    assert backoff * attempts < estimated_time < expected_time


def test_retry__on_exception(failed):
    on_exception = Mock()
    decorator = retry(RuntimeError, on_exception=on_exception)
    decorated = decorator(failed)
    with pytest.raises(RuntimeError):
        decorated()
    on_exception.assert_called_once()


@pytest.mark.asyncio()
async def test_retry__on_exception__async(async_failed):
    on_exception = AsyncMock()
    decorator = retry(RuntimeError, on_exception=on_exception)
    decorated = decorator(async_failed)
    with pytest.raises(RuntimeError):
        await decorated()
    on_exception.assert_called_once()
